A remake of the arcade/SNES game Dharma Dojo that I started making in [Akihabara](https://www.kesiev.com/akihabara/) but stopped caring about extremely quickly for some reason or another.

I did end up doing [a demake in PuzzleScript](https://www.puzzlescript.net/play.html?p=4e66fc851edbf42ae008) years later.