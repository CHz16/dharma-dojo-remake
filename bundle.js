{
    addImage:[
        ["logo", "font.png"],
        ["font", "font.png"],
        ["stripes", "stripes.png"],
        ["tiles", "tiles.png"]
    ],
    
    addFont:[
        {id: "small", image: "font", firstletter: " ", tileh: 8, tilew: 8, tilerow: 255, gapx: 0, gapy: 0},
        {id: "blue", image: "font", firstletter: " ", tileh: 8, tilew: 8, tilerow: 255, gapx: 0, gapy: 32}
    ],
    
    addTiles:[
        {id: "tile1", image: "tiles", tileh: 16, tilew: 16, tilerow: 6, gapx: 0, gapy: 0},
        {id: "tile2", image: "tiles", tileh: 16, tilew: 16, tilerow: 6, gapx: 0, gapy: 16}
    ]
}
