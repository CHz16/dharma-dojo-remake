var maingame;

gbox.onLoad(loadResources);


function loadResources() {
    help.akihabaraInit({
        title: "***GAME TITLE WOOP WOOP***"
    });
    
    gbox.addBundle({file: "bundle.js"});
    
    gbox.loadAll(main);
}

function main() {
    gbox.setGroups(["background", "gamecycle"]);
    maingame = gamecycle.createMaingame("gamecycle", "gamecycle");
    
    maingame.menuDatas = {
        main: {x: 10, y: 10, items: ["Journey", "Single", "Tutorial"]},
        journey: {x: 10, y: 20, items: ["one", "two"]},
        single: {x: 10, y: 20, items: ["three", "four"]}
    };
    maingame.currentMenu = "";
    maingame.changeMenu = function(menu) {
        toys.resetToy(this, "menu");
        this.currentMenu = menu;
    }
    maingame.menu = function() {
        var menuData = this.menuDatas[this.currentMenu];
        return toys.ui.menu(this, "menu", {font: "small", keys: {up: "up", down: "down", ok: "a", cancel: "b"}, selector: ">", items: menuData.items, x: menuData.x, y: menuData.y})
    }
    
    maingame.frameCount = 0;
    maingame.incrementFrame = function() {
        this.frameCount = this.frameCount + 1;
    };
    
    maingame.gameTitleIntroAnimation = function(reset) {
        this.incrementFrame();
        
        if (reset) {
            toys.resetToy(this, "logozoom");
        }
        
        gbox.blitFade(gbox.getBufferContext(), {alpha: 1, color: "rgb(255, 127, 0)"});
        gbox.blitAll(gbox.getBufferContext(), gbox.getImage("stripes"), {dx: -(this.frameCount % 30), dy: 0});
        gbox.blitAll(gbox.getBufferContext(), gbox.getImage("stripes"), {dx: 240 - this.frameCount % 30, dy: 0});
        
        toys.logos.zoomout(this, "logozoom", {image: "logo", x: gbox.getScreenHW()-gbox.getImage("logo").hwidth, y: 20, speed: 0.08, zoom: 3});
    };
    
    maingame.gameMenu = function(reset) {
        if (reset) {
            this.changeMenu("main");
        } else {
            gbox.blitFade(gbox.getBufferContext(), {alpha: 0.5});
            
            var madeSelection = this.menu();
            var cancelled = (toys.getToyValue(this, "menu", "ok") == -1);
            var selection = toys.getToyValue(this, "menu", "selected");
            
            if (this.currentMenu == "main") {
                if (madeSelection) {
                    if (cancelled) {
                        return -1;
                    } else {
                        if (selection == 0) {
                            // journey
                            this.changeMenu("journey");
                        } else if (selection == 1) {
                            // single
                            this.changeMenu("single");
                        } else if (selection == 2) {
                            // tutorial
                            return true;
                        }
                    }
                }
            } else if (this.currentMenu == "journey") {
                gbox.blitText(gbox.getBufferContext(), {font: "blue", text: "Journey", dx: 10, dy: 10});
                
                if (madeSelection) {
                    if (cancelled) {
                        this.changeMenu("main");
                    } else {
                        return true;
                    }
                }
            } else if (this.currentMenu == "single") {
                gbox.blitText(gbox.getBufferContext(), {font: "blue", text: "Single", dx: 10, dy: 10});
            
                if (madeSelection) {
                    if (cancelled) {
                        this.changeMenu("main");
                    } else {
                        return true;
                    }
                }
            }
            
            return false;
        }
    };
    
    gbox.go();
}
